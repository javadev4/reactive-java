package com.nilcompany.reactivejava.users;

import lombok.Builder;
import lombok.Data;

@Data
@Builder(toBuilder = true)
public class User {
	private int id;
	private String firstName;
	private String lastName;
}
