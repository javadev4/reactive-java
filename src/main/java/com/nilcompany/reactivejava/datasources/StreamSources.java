package com.nilcompany.reactivejava.datasources;

import java.util.stream.Stream;

import com.nilcompany.reactivejava.users.User;

public class StreamSources {

	public static Stream<String> stringNumbersStreams() {
		return Stream.of("one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten");
	}

	public static Stream<Integer> intNumbersStream() {
		return Stream.iterate(0, i -> i + 2).limit(10);
	}

	public static Stream<User> userStream() {
		return Stream.of(User.builder().id(1).firstName("Lionel").lastName("Messi").build(),
				User.builder().id(2).firstName("Cristiano").lastName("Ronaldo").build(),
				User.builder().id(3).firstName("Diego").lastName("Maradona").build(),
				User.builder().id(4).firstName("Zinedine").lastName("Zidane").build(),
				User.builder().id(5).firstName("Jürgen").lastName("Klinsmann").build(),
				User.builder().id(6).firstName("Gareth").lastName("Bale").build());
	}
}
