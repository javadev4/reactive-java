package com.nilcompany.reactivejava.excercise;

import java.io.IOException;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nilcompany.reactivejava.datasources.ReactiveSources;
import com.nilcompany.reactivejava.users.User;

public class Exercise4 {

	private static Logger log = LoggerFactory.getLogger(Exercise4.class);

	public static void main(String[] args) throws IOException {

		// Use ReactiveSources.intNumberMono()

		log.info("\nPrint the value from intNumberMono when it emits");
		ReactiveSources.intNumberMono().subscribe(System.out::println);

		log.info("\nGet the value from the Mono into an integer variable");
		Optional<Integer> number = ReactiveSources.intNumberMono().blockOptional();
		System.out.println("Number : " + number.get());

		User user = ReactiveSources.userMono().block();
		System.out.println(user);

		System.out.println("Press a key to end");
		System.in.read();
	}
}
