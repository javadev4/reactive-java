package com.nilcompany.reactivejava.excercise;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nilcompany.reactivejava.datasources.ReactiveSources;

public class Exercise3 {

	private static Logger log = LoggerFactory.getLogger(Exercise3.class);

	public static void main(String[] args) throws IOException {

		// Use ReactiveSources.intNumbersFlux()
		log.info(
				"\nGet all numbers in the ReactiveSources.intNumbersFlux stream into a List and print the list and its size");
		List<Integer> list = ReactiveSources.intNumbersFlux().toStream().collect(Collectors.toList());
		System.out.println("Number List : " + list);
		System.out.println("List Count : " + list.size());
	}
}
