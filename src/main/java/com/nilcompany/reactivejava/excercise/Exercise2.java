package com.nilcompany.reactivejava.excercise;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nilcompany.reactivejava.datasources.ReactiveSources;

public class Exercise2 {

	private static Logger log = LoggerFactory.getLogger(Exercise2.class);

	public static void main(String[] args) throws IOException {

		// Use ReactiveSources.intNumbersFlux() and ReactiveSources.userFlux()

		log.info("\nPrint all numbers in the ReactiveSources.intNumbersFlux stream");
		ReactiveSources.intNumbersFlux().subscribe(System.out::println);

		log.info("\nPrint all users in the ReactiveSources.userFlux stream");
		ReactiveSources.userFlux().subscribe(System.out::println);

		System.out.println("Press a key to end");
		System.in.read();
	}
}
