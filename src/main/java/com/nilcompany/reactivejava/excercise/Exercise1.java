package com.nilcompany.reactivejava.excercise;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nilcompany.reactivejava.datasources.StreamSources;
import com.nilcompany.reactivejava.users.User;

public class Exercise1 {
	private static Logger log = LoggerFactory.getLogger(Exercise1.class);

	public static void main(String[] args) {

		log.info("Print all numbers in the intNumbersStream stream");
		StreamSources.intNumbersStream().forEach(System.out::println);

		log.info("\nPrint numbers from intNumbersStream that are less than 5");
		StreamSources.intNumbersStream().filter(num -> num < 5).forEach(System.out::println);

		log.info("\nPrint the second and third numbers in intNumbersStream that's greater than 5");
		StreamSources.intNumbersStream().filter(num -> num > 5).limit(3).skip(1).forEach(System.out::println);

		log.info("\nPrint the first number in intNumbersStream that's greater than 5."
				+ "\n If nothing is found, print -1");
		System.out.println(StreamSources.intNumbersStream().filter(num -> num > 5).findFirst().orElse(-1));

		log.info("\n Print first names of all users in userStream");
		StreamSources.userStream().map(User::getFirstName).forEach(System.out::println);

		log.info("\n Print first names in userStream for users that have IDs from number stream");
		StreamSources.intNumbersStream().flatMap(num -> StreamSources.userStream().filter(user -> user.getId() == num))
				.map(User::getFirstName).forEach(System.out::println);
	}

}
