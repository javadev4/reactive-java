package com.nilcompany.reactivejava.excercise;

import java.io.IOException;
import java.util.concurrent.Flow.Subscription;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nilcompany.reactivejava.datasources.ReactiveSources;

import reactor.core.publisher.BaseSubscriber;

public class Exercise5 {

	private static Logger log = LoggerFactory.getLogger(Exercise5.class);

	public static void main(String[] args) throws IOException {

		// Use ReactiveSources.intNumberMono() and ReactiveSources.userMono()

		log.info("\nSubscribe to a flux using the error and completion hooks");
		ReactiveSources.intNumberMono().subscribe(System.out::println, err -> System.out.println(err.getMessage()),
				() -> System.out.println("Complete"));

		ReactiveSources.intNumbersFlux().subscribe(System.out::println, err -> System.out.println(err.getMessage()),
				() -> System.out.println("Complete"));

		log.info("\nSubscribe to a flux using an implementation of BaseSubscriber");
		ReactiveSources.intNumbersFlux().subscribe(new MySubscriber<>());
		System.out.println("Press a key to end");
		System.in.read();

	}
}

class MySubscriber<T> extends BaseSubscriber<T> {
	public void hookOnSubscribe(Subscription subscription) {
		System.out.println("Subscribe happened");
		request(1);
	}

	public void hookOnNext(T value) {
		System.out.println(value.toString() + " received");
		request(1);
	}
}